<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

#Route::get('/', function () {
    #return view('welcome');
#});


Route::get('/','HomeController@index');

Auth::routes();

Route::get('posts/edit/{post_id}','PostsController@getEdit');
Route::get('posts/meus','PostsController@getMeus');
Route::get('posts/create','PostsController@getCreate');
Route::post('posts/delete/{post_id}','PostsController@postDelete');
Route::post('posts/save/{post_id}','PostsController@postSave');


#Route::get('/home', 'HomeController@index');
