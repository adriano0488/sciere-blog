<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Route;
use Illuminate\Support\Facades\Input;
use App\Post;
use Auth;
class PostsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getMeus(){
        $user = Auth::user();
        $posts = \App\Post::with('user')
                ->where('user_id',$user->id)
                ->orderBy('created_at','desc')->paginate(10);
        
        return view('index',[
            'posts'=>$posts
        ]);
    }
    
    public function getEdit(){
        $post_id = Route::input('post_id');
        $post = \App\Post::find($post_id);
        
        if(!$post){
            session()->flash('error_message','Post não encontrado');
            return redirect('/');
        }
        
        if(!$post->isMine()){
            session()->flash('error_message','Você só pode editar seus próprios posts');
            return redirect('/');
        }
        
        return view('posts.form',[
            'post'=>$post
        ]);
        
    }
    
    public function getCreate(){
        return view('posts.form',[
        ]);
    }
    
    public function postSave(){
        $user = Auth::user();
        $post_id = Input::get('post_id');
        $titulo = Input::get('titulo');
        $conteudo = Input::get('conteudo');
        
        $is_new = true;
        if(!empty($post_id)){
            $post = Post::find($post_id);    
            if(!$post){
                $post = new Post();
            }else{
                $is_new = false;
            }
        }else{
            $post = new Post();
        }
        
        
        $atributos = [
            'titulo'=>$titulo,
            'conteudo'=>$conteudo,
            'user_id'=>$user->id
        ];
        
        foreach($atributos as $chave=>$valor){
            $post->$chave = $valor;
        }
        
        try{
            $post->save();
        }catch(\Exception $e){
            session()->flash('error_message','Ocorreu um erro ao salvar o POST');
            return redirect()->back();
        }
        
        session()->flash('success_message','Post salvo com sucesso');
        return redirect()->back();
    }
    
    
    public function postDelete(){
        $post_id = Route::input('post_id');
        
        $post = Post::find($post_id);
        
        if(!$post){
            session()->flash('error_message','Post não encontrado');
            return redirect()->back();
        }
        if(!$post->isMine()){
            session()->flash('error_message','Somente posts próprios podem ser removidos');
            return redirect()->back();
        }
        
        
        try{
            $post->delete();
        }catch(\Exception $e){
            session()->flash('error_message','Ocorreu um erro ao remover o POST.');
            return redirect()->back();
        }
        
        session()->flash('success_message','Post removido com sucesso.');
        return redirect()->back();
        
        
        
    }
    
    
}
