<div class='messages'>
    @if($message = session()->get('error_message'))
    <div class='alert alert-danger'>
        {!! $message !!}
    </div>
    @endif
    
    @if($message = session()->get('success_message'))
    <div class='alert alert-success'>
        {!! $message !!}
    </div>
    @endif
</div>
