<div class='posts'>
    <div class='posts--list'>
        @foreach($posts as $post)
        <article class='posts--list__item'>

            <h2 class='posts--list__titulo'>{{$post->titulo}} - {{$post->getKey()}}</h2>
            <div class='posts--list__content'>
                {!!$post->conteudo!!}
            </div>
            
            <div class='posts--list__author'>
                Autor: {{$post->user->name}}
            </div>
            
            @if($post->isMine())
            <div class='posts--list__footer'>
                <ul class='list-inline'>
                    <li class='list-inline-item'>
                        <a href='{{ url('posts/edit/'.$post->id)}}'>
                            Editar POST
                        </a>
                    </li>
                    <li class='list-inline-item'>
                        <form method='post' action='{{ url('/posts/delete/'.$post->id)}}'>
                            {{ csrf_field()}}
                            <a href='#' ng-click='deletePost($event.target)' data-id='{{ url('posts/edit/'.$post->id)}}'>
                                Apagar POST
                            </a>
                        </form>
                    </li>
                </ul>
            </div>
            @endif

        </article>
        @endforeach
    </div>
</div>


{{ $posts->render() }}