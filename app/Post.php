<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
class Post extends Model
{
    public function user(){
        return $this->hasOne(User::class,'id','user_id');
    }
    
    public function isMine(){
        $check = Auth::check();
        
        if($check){
            $user = Auth::user();
            return $user->id == $this->user_id;
        }
        
        return false;
    }
    
}
