<!DOCTYPE html>
<html>
    <head>

        <title>Sciere - Blog</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token()}}">
        <script src="http://code.angularjs.org/1.0.1/angular-1.0.1.min.js"></script>
        <link href="{{ asset('css/app.css')}}" rel="stylesheet">
        <link href="{{ asset('css/estilos.css')}}" rel="stylesheet">
        <link rel="stylesheet" href="{{ url('/css/bootstrap.min.css')}}" >
        <!-- Scripts -->
        <script>
            window.Laravel = {!! json_encode([
                    'csrfToken' => csrf_token(),
            ]) !!}
            ;
        </script>

        
        <script>
        var app = angular.module('blog',[]);
        
        app.controller('mainController',function($scope){
            $scope.toggleMenu = function($event){
                //var parent=angular.element($event.target); 
                var e = document.getElementById('dropdown-item-menu');
                e.classList.toggle('open');
            };
            $scope.deletePost = function(item){
                console.log(item);
                var c = confirm("Deseja realmente apagar este post?");
                
                if(c == true){
                    item.parentElement.submit();
                }
            }
            
        });
        </script>

    </head>
    <body ng-app='blog' ng-controller="mainController">

        <div id="app">
            <header>
                <div class="container">
                    <h1 class="float-l">
                        <a href="{{ url('/')}}" title="Sciere Blog">Sciere - Blog</a>
                    </h1>

                    <nav class="float-r">
                        <ul class="list-auto">
                            @if (Auth::guest())
                            <li><a href="{{ route('login')}}">Login</a></li>
                            <li><a href="{{ route('register')}}">Registrar</a></li>
                            @else
                            <li class="dropdown" id='dropdown-item-menu'>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" ng-click='toggleMenu(this)'>
                                    {{ Auth::user()->name }}
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout')}}"
                                           onclick="event.preventDefault();
                                                   document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout')}}" method="POST" style="display: none;">
                                            {{ csrf_field()}}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                            @endif
                        </ul>
                    </nav>
                </div>
            </header>

            <section class="content container">
                <div class="col-1 col-md-9">
                    @include('messages')
                    
                    @yield('content')
                </div>
                <aside class='col-md-3'>
                    <div class='widget'>
                        @include('auth._form_login')    
                    </div>
                    <div class='widget'>
                        <h3 class='title'>Gestão de Posts</h3>
                        <ul class='list-group'>
                            <li class='list-group-item'>
                                <a href='/posts/meus'>
                                    Meus Posts
                                </a>
                            </li>
                            <li class='list-group-item'>
                                <a href='/posts/create'>
                                    Criar Novo Post
                                </a>
                            </li>
                        </ul>
                    </div>
                    

                    @yield('sidebar-right')
                </aside>

            </section>

            <footer>

            </footer>



        </div>
    </body>
</html>
