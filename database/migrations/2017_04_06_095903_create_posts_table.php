<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('posts');
        Schema::create('posts', function (Blueprint $table) {
            #Schema::enableForeignKeyConstraints();
            $table->engine = 'InnoDB';


            #$table->integer('id')->unsigned()->index();
            #$table->primary('id');
            $table->increments('id')->unsigned()->index();
            $table->string('titulo');
            $table->string('conteudo',8000);
            $table->integer('user_id')->unsigned()->index();
            #$table->foreign('user_id')
                #->references('id')->on('users')
                #->onDelete('cascade')
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('posts');
        #Schema::table('posts', function (Blueprint $table) {
            //
        #});
    }
}
