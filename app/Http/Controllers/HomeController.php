<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth',[
            'except'=>[
                'index'
            ]
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $posts = \App\Post::with('user')->orderBy('created_at','desc')->paginate(10);
        
        return view('index',[
            'posts'=>$posts
        ]);
    }
}
