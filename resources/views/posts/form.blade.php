@extends('layouts.master')

@section('content')
<h2>Edição de POSTS</h2>

<div class='post'>
    <div class='post--form'>
        <form class='form-horizontal' method='POST' action='{{ url('posts/save')}}'>
            {{ csrf_field() }}
            
            <input type='hidden' name='post_id' value='{{ isset($post) ? $post->id : ''}}' />

            <div class='form-group'>
                <label class='control-label col-sm-2'>Título</label>
                <div class="col-sm-10">
                    <input class='form-control' type='text' name='titulo' value='{{ old('titulo',isset($post) ? $post->titulo : '') }}' />
                </div>
            </div>

            <div class='form-group'>
                <label class='control-label col-sm-2'>Conteúdo</label>
                <div class="col-sm-10">
                    <textarea name='conteudo' class='form-control'>{{ old('titulo',isset($post) ? $post->conteudo : '') }}</textarea>
                </div>
            </div>
            
            <div class='actions'>
                <button type="submit" class='btn btn-primary pull-right'>
                    Salvar
                </button>
            </div>
            
        </form>
    </div>
</div>
@stop